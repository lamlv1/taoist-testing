import { executeCommand, getPathCommandCucumber } from '../../common/common-functions';

const MODULE_NAME = 'login';
const pathScriptLogin = `src/features/login/login.feature`;

(async function main() {
  await executeCommand(getPathCommandCucumber([pathScriptLogin], MODULE_NAME, true));
})();
