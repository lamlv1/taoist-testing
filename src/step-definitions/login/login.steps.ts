import { LoginPage } from '../../page-objects/loginPage';
const loginPage = new LoginPage();
import * as dotenv from 'dotenv';
import { Given, When, Then } from '@cucumber/cucumber';

dotenv.config();

Given('I go to the tabist website', async function () {
  await loginPage.goToWebsite(this.page);
});

When('I input {string} and {string} in the form login', async function (emailValue, passwordValue) {
  await loginPage.inputInforAccount(this.page, emailValue, passwordValue);
});

Then('Verify successful login to the system', async function () {
  await loginPage.assertLoginSuccess(this.page);
});

Then('Verify {string} message is displayed in the page', async function (messageContain) {
  await loginPage.assertErrorMessage(this.page, messageContain);
});

Given('I log out of the system', async function () {
  await loginPage.logout(this.page);
});
