import { ICustomWorld } from '../../common/custom-world';
import { executeCommand, runReport } from '../../common/common-functions';
import {
  chromium,
  ChromiumBrowser,
  firefox,
  FirefoxBrowser,
  webkit,
  WebKitBrowser,
} from 'playwright';
import { Before, After, BeforeAll, AfterAll, setDefaultTimeout } from '@cucumber/cucumber';
import * as dotenv from 'dotenv';

dotenv.config();

let browser: ChromiumBrowser | FirefoxBrowser | WebKitBrowser;

declare global {
  // eslint-disable-next-line no-var
  var browser: ChromiumBrowser | FirefoxBrowser | WebKitBrowser;
}
setDefaultTimeout(process.env.PWDEBUG ? -1 : 60 * 1000);
// To launch the browser before all the scenarios
BeforeAll(async () => {
  switch (process.env.BROWSER) {
    case 'firefox':
      browser = await firefox.launch({ headless: false });
      break;
    case 'webkit':
      browser = await webkit.launch({ headless: false });
      break;
    default:
      browser = await chromium.launch({ headless: false });
  }
});

// To close the browser after all the scenarios
AfterAll(async function (this: ICustomWorld) {
  await Promise.resolve();
  setTimeout(
    async () => await executeCommand(runReport(process.argv[process.argv.length - 1])),
    5000,
  );
});

// Before every scenario, Create new context and page
Before(async function (this: ICustomWorld) {
  this.context = await browser.newContext();
  this.page = await this.context?.newPage();
});

// After every scenario, Close context and page
After(async function (this: ICustomWorld) {
  await this.page.close();
  await this.context.close();
});
