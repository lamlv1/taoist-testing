Feature: Login
  In order to login by account
  As a user
  I want to login with my account

  Scenario: Login Success
    Given I go to the tabist website
    When I input "<emailValue>" and "<passwordValue>" in the form login
    Then Verify successful login to the system
  Examples:
  |emailValue               |passwordValue|
  |nhidt@tokyotechlab.com       |ttlab123456 | 
