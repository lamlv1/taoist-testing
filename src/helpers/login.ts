import { LOCAL_STORAGE_KEYS, PAGE_URLS } from '../common/constants';
import { DYNAMIC_ELEMENT } from '../common/dynamicElement';
import { getLocalStorageInBrowser, setLocalStorageInBrowser } from '../common/common-functions';
import axios from 'axios';
import * as dotenv from 'dotenv';
import { Page } from 'playwright';

dotenv.config();
const URL_REFRESH_TOKEN = 'refresh-token';
const BUFFER_TIME = 60 * 1000;

export class Login {
  async callAPIRefreshToken() {
    const response = await axios({
      method: 'POST',
      baseURL: process.env.API_URL,
      url: URL_REFRESH_TOKEN,
      headers: {
        Authorization: `Bearer ${global.localStorage[LOCAL_STORAGE_KEYS.REFRESH_TOKEN]}`,
      },
    });
    return response?.data || {};
  }

  async allAPIRefreshToken() {
    const response = await axios({
      method: 'POST',
      baseURL: process.env.API_URL,
      url: URL_REFRESH_TOKEN,
      headers: {
        Authorization: `Bearer ${global.localStorage[LOCAL_STORAGE_KEYS.REFRESH_TOKEN]}`,
      },
    });
    return response?.data || {};
  }

  async loginByUsername(page: Page, email = process.env.EMAIL, password = process.env.PASSWORD) {
    await page.waitForSelector(DYNAMIC_ELEMENT.GMAIL_INPUT_TYPE('email'));
    await page.type(DYNAMIC_ELEMENT.GMAIL_INPUT_TYPE('email'), email);
    await page.waitForSelector(DYNAMIC_ELEMENT.GMAIL_INPUT_TYPE('password'));
    await page.type(DYNAMIC_ELEMENT.GMAIL_INPUT_TYPE('password'), password);
    await page.keyboard.press('Enter');
    await page.waitForNavigation();
  }
  async logout(page: Page) {
    await page.click(DYNAMIC_ELEMENT.PROFILE_NAME);
    await page.click(DYNAMIC_ELEMENT.LOGOUT);
    await page.waitForNavigation();
  }
}
