import { Login } from '../helpers/login';
import { changeLanguage, setLocaleI18n } from '../common/common-functions';
import { PAGE_URLS } from '../common/constants';
import { Page } from 'playwright';
import * as dotenv from 'dotenv';
import expect from 'expect';

dotenv.config();
const login = new Login();

export class LoginPage {
  async goToWebsite(page: Page) {
    await page?.goto(PAGE_URLS.LOGIN);
    await page?.waitForSelector('#root');
  }

  async clickLoginButton(page: Page) {
    await page.click('button.v-btn--text');
    await page.waitForNavigation();
  }

  async clickLinkAccount(page: Page) {
    await page.click("//div[contains(text(),'tài khoản')]//ancestor::div[@role='link']");
  }

  async inputInforAccount(page: Page, emailValue: string, passwordValue: string) {
    await login.loginByUsername(page, emailValue, passwordValue);
  }

  async assertLoginSuccess(page: Page) {
    // await page.waitForSelector('div.sidebar');
    expect(page.url()).toEqual(PAGE_URLS);
  }

  async assertErrorMessage(page: Page, messageContent: string) {
    await page.waitForSelector('#swal2-content');
    const message = await page.innerText('#swal2-content');
    await setLocaleI18n(page);
    expect(message).toEqual(messageContent);
  }

  async logout(page: Page) {
    await login.logout(page);
  }
}
